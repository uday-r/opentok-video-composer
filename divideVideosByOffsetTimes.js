
function divideVideosByOffsetTimes(script, format) {
  	var startTime = 10000000000000;
  	var endTime = 0;
  	// find start end end time for the whole playback
  	script.files.forEach(function(e) {
    	if (e.startTimeOffset < startTime) {
      		startTime = e.startTimeOffset;
    	}
	    if (e.stopTimeOffset > endTime) {
	      endTime = e.stopTimeOffset;
	    }
	});
  	// make them all 0 based
  	script.files.forEach(function(e) {
    	e.startTimeOffset -= startTime;
    	e.stopTimeOffset -= startTime;
  	});
  	// sort them by start time
  	script.files.sort(function(a, b) {
    	return a.startTimeOffset - b.startTimeOffset;
  	});
  	//console.log("duration=", endTime-startTime);
  	//console.log(script.files);

  	// calculate the number of streams on each interval
  	var timeline = {};
  	script.files.forEach(function(e) {
    	if (!timeline[e.startTimeOffset]) {
      		timeline[e.startTimeOffset] = {};
      		timeline[e.startTimeOffset].count = 1;
      		timeline[e.startTimeOffset].add = [];
    	} else {
      		timeline[e.startTimeOffset].count++;
    	}
    	timeline[e.startTimeOffset].add.push(e);

    	if (!timeline[e.stopTimeOffset]) {
      		timeline[e.stopTimeOffset] = {};
      		timeline[e.stopTimeOffset].count = -1;
      		timeline[e.stopTimeOffset].remove = [];
    	} else {
      		timeline[e.stopTimeOffset].count--;
    	}
    	timeline[e.stopTimeOffset].remove.push(e);
  	});

  	var keypoints = timeline;
  	var current = 0;
  	var prev = -1;

  	//console.log(keypoints);
  	var active = {};
  	var composer = [];

  	var addStreams = function(active, streams) {
    	if (streams) {
      		streams.forEach(function(stream) {
        		active[stream.streamId] = {
          			offset: stream.startTimeOffset, 
          			name: stream.connectionData,
          			videoType: stream.videoType
        		};
      		});
    	}
  	};

  	var removeStreams = function(active, streams) {
    	if (streams) {
      		streams.forEach(function(stream) {
        		delete active[stream.streamId];
      		});
    	}
  	};

  	for (var i in keypoints) {

    	// on first iteration we don't output anything 
    	if (prev == -1) {
      		prev = i;
      		addStreams(active, keypoints[i].add);
      		continue;
    	}

	    // before adding the new entry we output the diff between previous and current time
	    //console.log("t=[" + prev + ", " + i + "]");
    	var entry = { start: prev, end: i, files: []};
    	for (var j in active) {
      		//console.log("streamOffset="+(prev-active[j]), j);
	      	entry.files.push({streamId: j, offset: (prev-active[j].offset), name: active[j].name, videoType: active[j].videoType});
	    }
    	composer.push(entry);

	    // now update
	    current += keypoints[i].count;
	    addStreams(active, keypoints[i].add);
	    removeStreams(active, keypoints[i].remove);
	    prev = i;
  	}

  	return composer;
}

module.exports = {
	divideVideosByOffsetTimes: divideVideosByOffsetTimes,
}