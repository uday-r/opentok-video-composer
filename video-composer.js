#!/usr/bin/env node

var fs = require('fs'),
    util = require('util'),
    exec = require('child_process').execSync,
    fs = require('fs'),
    program = require('commander'),
    path = require('path'),
    unzip = require('unzip'),
    child;
program
    .version('0.0.1')
    .usage('[options] -i <zipFile>')
    .option('-i, --input <zipFile>',  'Archive ZIP file')
    .option('-f, --format [type]', 'Output format [webm,mp4]', 'webm')
    .parse(process.argv);

if (!program.input) {
  program.help();
}

var zip_file = program.input;
var dirname = path.dirname(zip_file);
var basename = path.basename(zip_file, ".zip");
var temp_dir = path.join(dirname, basename);
var format = program.format;
// Unzip the archive
var input = fs.createReadStream(zip_file);
var result = input.pipe(unzip.Extract({ path: temp_dir }));

result.on("close", function() {
  var files = fs.readdirSync(temp_dir);

  var json_file;
  files.forEach(function(file) {
    if (path.extname(file) == ".json") {
      json_file = file;
    }
  });

  if (!json_file) {
    console.log("ZIP file does not contain a json file");
  }


  var script = JSON.parse(fs.readFileSync(path.join(temp_dir, json_file)).toString());
  //This loop converts all mkv files to mp4. START
  var { convertAllVideosToMp4 } = require('./convertAllVideosToMp4');
  script = convertAllVideosToMp4(script, basename);
  //This loop converts all mkv files to mp4. END

  script.files = script.files.reduce((acc, it) => {
    if(!it.smallFile) {
      return acc.concat(it);
    }
    return acc;
  }, [])

  const { divideVideosByOffsetTimes } = require('./divideVideosByOffsetTimes');
  var composer = divideVideosByOffsetTimes(script, format);
 
  //DUP func
  var checkIfValidAudio = function(file) {
    cmd = "ffmpeg  -threads 10 -loglevel quiet -err_detect ignore_err -i " + file + " -c copy  -preset ultrafast  " + basename + "/" + (Math.random() * 1000) + ".mp4";
    try {
      exec(cmd);
    } catch (e) {
      console.log("ERR: ", e);
      if(e) {
        return false;
      }
    }
    return true;
  }

  var archiveId = script.id;
  var archive_path = temp_dir;
  var chunks = [];
  var cmd;
  // now ready to rock
  for (var c in composer) {
    var e = composer[c];
    var filter = "";
    if (e.files.length == 1) {
      filter = " -vf scale=1280:720 -max_muxing_queue_size 1024 "
    } else if (e.files.length == 2) {
      filter = "-filter_complex \"[0]scale=-1:720,pad=2*iw:2*ih:0:300[left];[1]scale=-1:720[right];[left][right]overlay=main_w/2:300,scale=1280:720"
    } else if (e.files.length == 3) {
      filter = "-filter_complex \"[0]scale=-1:720[a];[1]scale=-1:720[b];[2]scale=-1:720[c];[a]pad=2360:1440[x];[x][b]overlay=1280:0[y];[y][c]overlay=640:720";
    } else if(e.files.length == 4) {
      filter = "-filter_complex \"[0]scale=-1:720[a];[1]scale=-1:720[b];[2]scale=-1:720[c];[3]scale=-1:720[d];[a]pad=2360:1440[x];[x][b]overlay=1280:0[y];[y][c]overlay=0:720[z];[z][d]overlay=1280:720";
    } else if(e.files.length == 5) {
      filter = "-filter_complex \"[0]scale=-1:720[a];[1]scale=-1:720[b];[2]scale=-1:720[c];[3]scale=-1:720[d];[4]scale=-1:720[e];[a]pad=3640:1440[z];[z][b]overlay=1280:0[y];[y][c]overlay=2560:0[x];[x][d]overlay=640:720[w];[w][e]overlay=1920:720"
    } else if(e.files.length == 5) {
      filter = "-filter_complex \"[0]scale=-1:720[a];[1]scale=-1:720[b];[2]scale=-1:720[c];[3]scale=-1:720[d];[4]scale=-1:720[e];[a]pad=3640:1440[z];[z][b]overlay=1280:0[y];[y][c]overlay=2560:0[x];[x][d]overlay=640:720[w];[w][e]overlay=1920:720"
    } else if(e.files.length == 6) {
      filter = "-filter_complex \"[0]scale=-1:720[a];[1]scale=-1:720[b];[2]scale=-1:720[c];[3]scale=-1:720[d];[4]scale=-1:720[e];[5]scale=-1:720[f];[a]pad=3640:1440[z];[z][b]overlay=1280:0[y];[y][c]overlay=2560:0[x];[x][d]overlay=0:720[w];[w][e]overlay=1280:720[v];[v][f]overlay=2560:720"
    } else if(e.files.length == 7) {
      filter = "-filter_complex \"[0]scale=-1:720[a];[1]scale=-1:720[b];[2]scale=-1:720[c];[3]scale=-1:720[d];[4]scale=-1:720[e];[5]scale=-1:720[f];[6]scale=-1:720[g];[a]pad=3640:1440[z];[z][b]overlay=1280:0[y];[y][c]overlay=2560:0[x];[x][d]overlay=3840:0[w];[w][e]overlay=1280:720[v];[v][f]overlay=2560:720[u];[u][g]overlay=3800:720"
    }
    if (e.files.length > 1) {
      filter += "\" ";
    }
    cmd = "ffmpeg -y -threads 10 -loglevel quiet ";
    var minDuration = 100000;
    var audioMerge = [];
    for (var s in e.files) {
      var stream = e.files[s];
      if(e.files[s].videoType == "camera") {
        const arrInd = audioMerge.length > 0? audioMerge.length: 0;
        audioMerge.push(`[${arrInd}:a]`)
      }
      var name = "name";
      var duration = (e.end - stream.offset)/1000;
      minDuration = minDuration < duration ? minDuration : duration;
      cmd += util.format("-ss %d -t %d -i " + archive_path + "/%s.mp4  ", 
                          stream.offset/1000, 
                          (e.end - stream.offset)/1000, 
                          stream.streamId);
    }

    var chunk = util.format("temp-%d-%d", e.start, e.end);
    chunks.push(chunk);
    var audioCmd
    if(audioMerge.length > 0) {
      audioCmd = cmd + " -filter_complex \"" + audioMerge.join("") + `amerge=inputs=${audioMerge.length}[a] \"` + " -map '[a]'  -preset superfast  " + basename + "/" + chunk + "audio_only.mp4";
    }
    var videoCmd = cmd + filter + " -shortest  -preset superfast " + basename + "/" + chunk + "video_only.mp4";
    //Attribute `-an` could cause problem.
    console.log("Running video merge: ", videoCmd);
    child = exec(videoCmd, function(err, stdout, stderr) {
      if (err) {
        console.log(err);
        process.exit(1);
      }
    });
    if(audioCmd) {
      console.log("Running audio merge: ", audioCmd);
      child = exec(audioCmd, function(err, stdout, stderr) {
        if (err) {
          console.log(err);
          process.exit(1);
        }
      });
      if(checkIfValidAudio(basename + "/" + chunk + "audio_only.mp4")) {
        var combineAudioVideo = "ffmpeg  -threads 10 -loglevel quiet -i "
          + basename + "/" + chunk + "video_only.mp4 "
          + " -i "
          + basename + "/" + chunk + "audio_only.mp4 "
          + " -c copy -map 0:v -map 1:a  -preset superfast "
          + basename + "/" + chunk + ".mp4";
        console.log("Merging audio and video: ", combineAudioVideo);
        child = exec(combineAudioVideo, function(err, stdout, stderr) {
          if (err) {
            console.log(err);
            // process.exit(1);
          }
        });
      } else {
        //Dup code.
        var mvCmd = "mv "
        + basename + "/" + chunk + "video_only.mp4 "
        + basename + "/" + chunk + ".mp4 ";
        console.log("Moving video only to mixed file: ", mvCmd);
        child = exec(mvCmd, function(err, stdout, stderr) {
          if (err) {
            console.log(err);
            process.exit(1);
          }
        });
      }
    } else {
      var mvCmd = "mv "
      + basename + "/" + chunk + "video_only.mp4 "
      + basename + "/" + chunk + ".mp4 ";
      console.log("Moving video only to mixed file: ", mvCmd);
      child = exec(mvCmd, function(err, stdout, stderr) {
        if (err) {
          console.log(err);
          process.exit(1);
        }
      });
    }
  }

  console.log("---------------------")
  console.log("CHUnks: ", chunks);
  console.log("---------------------")

  chunks = chunks.reduce(function(acc, it, id) {
    if(checkIfValidAudio(basename + "/" + it + "." + format)) {
      return acc.concat([it]);
    }
    return acc;
  }, []);

  console.log("---------------------")
  console.log("CHUnks: ", chunks);
  console.log("---------------------")


  var content = "file '" + chunks.join("\nfile '") + ".mp4'";
  fs.writeFileSync(basename + "/" + archiveId + "-list.txt", content);

  let concatFileNames = chunks.reduce(function(acc, it) {
    return acc + " -i " + basename + "/" + it + ".mp4" + " ";
  }, "");

  let concatPrimaryFilter = chunks.reduce(function(acc, it, id) {
    return acc + "[" + id + ":v]scale=1280:720,setdar=w/h[v" + id + "];"
  }, "");

  let concatSecondaryFilter = chunks.reduce(function(acc, it, id) {
    return acc + "[v" + id + "][" + id + ":a]";
  }, "");

  console.log("-----------------------");
  console.log("Filter2: ", concatSecondaryFilter);
  console.log("-----------------------");

  concatSecondaryFilter = concatSecondaryFilter + "concat=n=" + chunks.length + ":v=1:a=1[v][a]"
  console.log("-----------------------");
  console.log("Filters: ", concatPrimaryFilter);
  console.log("Filter2: ", concatSecondaryFilter);
  console.log("-----------------------");
  let concatFilter = concatPrimaryFilter + concatSecondaryFilter;
  // concat all chunks
  console.log("Combining all temp videos");
  cmd = "ffmpeg -y -threads 10 -loglevel quiet " + concatFileNames + " -r 24 " + " -filter_complex \"" + concatFilter + "\" -map [v] -map [a]  -preset superfast  " + basename + "/archive." + format;
  console.log(cmd);
  child = exec(cmd, function(error, stdout, stderr) {
    if (error !== null) {
      console.log('exec error: ' + error);
    }
  });
  console.log("Combined temp videos into single file.")


  let imgCmd = `ffmpeg -loglevel quiet -ss 0 -i ${basename}/archive.mp4 -vframes 1 -q:v 2 ${basename}/archive.png`;
  console.log(imgCmd);
  child = exec(imgCmd, function(error, stdout, stderr) {
    if (error !== null) {
      console.log('exec error: ' + error);
    }
  });

  script.files.forEach(function(fileOb) {
     imgCmd = `ffmpeg -loglevel quiet -ss 0 -i ${basename}/${fileOb.streamId}.mp4 -vframes 1 -q:v 2 ${basename}/${fileOb.streamId}.png`;
     child = exec(imgCmd, function(error, stdout, stderr) {
        if (error !== null) {
          console.log('exec error: ' + error);
        }
      });
  })

  // remove temp files
  try {
    console.log("Deleting temp files.");
    fs.unlinkSync(basename + "/" + archiveId + "-list.txt");
    chunks.forEach(function(chunk) {
      console.log("Removing", chunk);
      fs.unlinkSync(basename + "/" + chunk + "." + format);
      try {
        fs.unlinkSync(basename + "/" + chunk + "video_only." + format);
        fs.unlinkSync(basename + "/" + chunk + "audio_only.mp4");
      } catch(e) {
        console.log("Attemped deleting unknown temp file", e);
      }
    });
    fs.readdir(archiveId, (err, files)=>{
       console.log("Files", files);
       for (var i = 0, len = files.length; i < len; i++) {
          console.log("File: ", files[i]);
          var match = files[i].match(/(temp.*|\d*.?\d+)\.mp4/);
          console.log(match);
          if(match !== null) {
              fs.unlink(basename + "/" + match[0], function(err) {
                if(err) {
                  console.log("Error deleting " + match);
                } else {
                  console.log("Deleted " + match);
                }
              });
          }
       }
    });
    console.log("Done deleting...")
  } catch(e) {
    console.log("Attempted to delete missing file, recovered from that.")
    console.log(e);
  }
});