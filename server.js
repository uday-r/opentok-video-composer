const express = require('express');
const app = express();
const fork = require('child_process').fork;
const request = require('request');
const path = require('path');
const exec = require('child_process').execSync;

app.listen(10000, () => console.log(`Your app listening on port 10000!`));

app.use('/generate-thumbnail', function(req, res) {
	const {
		fileName,
		subdomainName,
	} = req.query;
	if(!fileName || !subdomainName) {
		console.log("INPUT invalid");
		return;
	}
	console.log("Generating thubnail for ", fileName);
	let imgCmd = `ffmpeg -loglevel quiet -ss 2 -i ${fileName}.mp4 -vframes 1 -q:v 2 ${fileName}.png`;
	console.log(imgCmd);
	process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
	child = exec(imgCmd, function(error, stdout, stderr) {
		if (error) {
		    console.log("Error composing audio", err);
			request(`https://${subdomainName}.aerialspac.es/api/room/archiveProcessingError?archiveId=${fileName}&isSecure=true`)
	    }
	});
	console.log("Done generating thumbnail...");
	setTimeout(function() {
		request(`https://${subdomainName}.aerialspac.es/api/room/thumbnailGenerated?archiveId=${fileName}&isSecure=true`);
	}, 0);
	res.send({
		message: "You'll be notified once your request is addressed....",
	})
});

app.use('/', function(req, res) {
	const {
		fileName,
		subdomainName,
	} = req.query;
	if(!fileName || !subdomainName) {
		console.log("INPUT invalid");
		return;
	}
	var basename = path.basename(fileName, ".zip");
	console.log(`fileName: ${fileName}`);
	console.log(`subdomainName: ${subdomainName}`);
	console.log(`basename: ${basename}`);
	const ps = fork("./video-composer.js", ["-i", fileName, "-f", "mp4"]);
	ps.on("exit", function(err, stdout, stderr) {
		console.log("Done composing video and audio...");
	    process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
		if (err) {
		    console.log("Error composing audio", err);
			request(`https://${subdomainName}.aerialspac.es/api/room/archiveProcessingError?archiveId=${basename}&isSecure=true`)
	    } else {
			request(`https://${subdomainName}.aerialspac.es/api/room/individualArchiveIsUploaded?archiveId=${basename}&isSecure=true`)
	    }
	});
	res.send({
		message: "You'll be notified once your request is addressed....",
	})
})

