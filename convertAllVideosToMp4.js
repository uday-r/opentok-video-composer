const exec = require('child_process').execSync;
var fs = require('fs');

function convertAllVideosToMp4(script, basename) {
  script.files = script.files.map((file)=> {
    try {
      fs.unlinkSync(basename + "/" + file.streamId + ".mp4", function(err) {
        console.log('deleted file', err);
      });  
    } catch(e) {
      console.log("Error deleting file: ", file.filename);
    }
    let cmd= "ffmpeg -threads 10 -loglevel quiet  -acodec libopus  -i " + basename + "/" + file.filename + " -vf scale=1280:720 -max_muxing_queue_size 99999999  -b:a 200k -preset superfast  " + basename + "/" + file.streamId + ".mp4";
    try {
      child = exec(cmd, function(err, stdout, stderr) {
        if (err) {
          console.log(err);
          file.smallFile = true;
          // process.exit(1);
        }
      });
      if(file.videoType == "screen") {
        cmd = "ffmpeg -threads 10 -loglevel quiet -i " + basename + "/" + file.streamId + ".mp4 -f lavfi -i anullsrc -c:v copy -map 0:v -map 0:a? -map 1:a -shortest  -max_muxing_queue_size 1024  -preset superfast " + basename + "/" + file.streamId + "-tmp.mp4";
        child = exec(cmd, function(err, stdout, stderr) {
          if (err) {
            console.log(err);
            file.smallFile = true;
            // Donot use this file for merging.
            // process.exit(1);
          }
        });
        if(!file.smallFile) {
          fs.unlinkSync(basename + "/" + file.streamId + ".mp4", function(err) {
            if(!err) {
              console.log('Deleting ' + basename + "/" + file.streamId + ".mp4");
            } else {
              console.log('Error deleting ' + basename + "/" + file.streamId + ".mp4");
            }
          });
          exec("mv " + basename + "/" + file.streamId + "-tmp.mp4  " + basename + "/" + file.streamId + ".mp4 ", function(err, stdout, stderr) {
            if (err) {
              console.log(err);
              process.exit(1);
            }
          });
        }
      }
    } catch (e) {
      file.smallFile = true;
    }
    return file;
  });
  return script;
}

module.exports = {
  convertAllVideosToMp4: convertAllVideosToMp4
};